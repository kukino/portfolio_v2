import { ResponseProps } from '@/slicers/common'
import { useSelector } from 'react-redux'
import { RootState } from '@/app/rootReducer'

export const useAsssertResponse = (data: ResponseProps) => {
  const { response } = useSelector((state: RootState) => state.commonState)

  return (
    response.method === data.method &&
    response.status === data.status &&
    response.url === data.url
  )
}
