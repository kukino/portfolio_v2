import { AppThunk } from '@/app/store'
import { setFlashError } from '@/slicers/common'
import { setLoginUserId } from '@/slicers/common'
import { userGetSuccess, User } from '@/slicers/users'
import { sleep, apiLatency } from '@/utils'
import { createUser } from '@/slicers/users/mock'

type LoginProps = {
  name: string
  password: string
}

const authRequest = {
  login: () => ({
    request: (data: LoginProps): AppThunk => async dispatch => {
      await sleep(apiLatency)

      dispatch(
        userGetSuccess({
          id: 1,
          name: 'テストユーザー',
          email: data.name
        })
      )
      dispatch(setLoginUserId(1))
    }
  }),

  fetchLoginUser: () => ({
    request: (): AppThunk => async dispatch => {
      await sleep(apiLatency)
    }
  }),

  logout: () => ({
    request: (): AppThunk => async dispatch => {
      await sleep(apiLatency)

      dispatch(setLoginUserId(null))
    }
  })
}

export default authRequest
