import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import store from './app/store'
import ProviderWrapper from '@/provider'
import {
  ThemeProvider,
  createMuiTheme,
  StylesProvider
} from '@material-ui/core/styles'

const AppWrapper: React.FC = ({ children }) => {
  return (
    <ProviderWrapper store={store()}>
      <StylesProvider injectFirst>
        <ThemeProvider theme={createMuiTheme({})}>{children}</ThemeProvider>
      </StylesProvider>
    </ProviderWrapper>
  )
}

export default AppWrapper
