import React from 'react'
import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import { WithIconButton } from './index'
import AppWrapper from '@/AppWrapper'
import { Person } from '@material-ui/icons'

const setup = async (loading: boolean) => {
  const utils = render(
    <AppWrapper>
      <WithIconButton icon={Person} loading={loading} color="error">
        test
      </WithIconButton>
    </AppWrapper>
  )

  const queryButton = utils.getByRole('button')
  const queryIcon = utils.queryByTestId('icon')
  const queryLoadingIcon = utils.queryByTestId('loadingIcon')

  return {
    utils,
    queryIcon,
    queryLoadingIcon,
    queryButton
  }
}

test('withIconoButton loading=false', async () => {
  const utils = await setup(false)

  expect(utils.queryButton).not.toHaveAttribute('disabled')

  expect(utils.queryLoadingIcon).toBeNull()
  expect(utils.queryIcon).toBeInTheDocument()
})

test('withIconoButton loading=true', async () => {
  const utils = await setup(true)

  expect(utils.queryButton).toHaveAttribute('disabled')

  expect(utils.queryLoadingIcon).toBeInTheDocument()
  expect(utils.queryIcon).toBeNull()
})
