import React from 'react'
import ButtonList from './index'
import { withRedux, withStyles } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
import { CreateButton } from '../Buttons'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  component: ButtonList,
  decorators: [withRedux(preloadState), withStyles({ width: '100vw' })],
  includeStories: /.*Story$/
}

const Test = () => null

export const defaultStory = () => (
  <ButtonList>
    <CreateButton />
    <CreateButton />
    <Test />
    <Test />
    <CreateButton />
  </ButtonList>
)
