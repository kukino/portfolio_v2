import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      '&>div:empty': {
        display: 'none'
      }
    }
  })
)

export default useStyles
