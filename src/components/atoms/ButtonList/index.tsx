import React from 'react'
import { Box } from '@material-ui/core'
import useStyles from './styles'

type ButtonListProps = {
  children: React.ReactNode[]
}

const ButtonList: React.FC<ButtonListProps> = ({ children }) => {
  const classes = useStyles()
  return (
    <Box display="flex" justifyContent="center" className={classes.container}>
      {children
        .filter(child => !!child)
        .map(child => (
          <Box mx={1}>{child}</Box>
        ))}
    </Box>
  )
}

export default ButtonList
