import React from 'react'
import ProviderWrapper from '@/provider'
import store from '@/app/store'
import { RootState } from '@/app/rootReducer'
import { withLoginState } from '@/slicers/users/mock'
import { DeepPartial } from '@reduxjs/toolkit'
import deepmerge from 'deepmerge'
import AppContainer from './ecosystem/AppContainer'
import { initialCommonState } from '@/slicers/common'
import { initialUserState } from '@/slicers/users'

export const withStyles = (styles: React.CSSProperties) => (
  storyFn: Function
) => <div style={styles}>{storyFn()}</div>

export const withAppContainer = () => (storyFn: Function) => (
  <AppContainer>{storyFn()}</AppContainer>
)

export const withRedux = (preloadState: DeepPartial<RootState> = {}) => (
  storyFn: Function
) => (
  <ProviderWrapper store={store(deepmerge(initialState, preloadState))}>
    {storyFn()}
  </ProviderWrapper>
)

export const withLogin = (preloadState: DeepPartial<RootState> = {}) => (
  storyFn: Function
) => {
  return (
    <ProviderWrapper
      store={store(deepmerge.all([initialState, withLoginState, preloadState]))}
    >
      {storyFn()}
    </ProviderWrapper>
  )
}

const initialState: RootState = {
  commonState: initialCommonState,
  usersState: initialUserState
}
