import React from 'react'
import AppSideBar from './index'
import { withRedux } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { sideBarWidth } from '@/components/ecosystem/AppContainer'
import { action } from '@storybook/addon-actions'
import { DeepPartial } from '@reduxjs/toolkit'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  component: AppSideBar,
  decorators: [withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const _defaultStory = () => (
  <AppSideBar
    sideBarWidth={sideBarWidth}
    sideBarOpened={true}
    onSideBarClose={action('clicked')}
  ></AppSideBar>
)
