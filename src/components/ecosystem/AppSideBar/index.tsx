import React from 'react'
import { Divider, Drawer, IconButton, List } from '@material-ui/core'
import { useTheme } from '@material-ui/core/styles'
import { ChevronLeft, ChevronRight, People } from '@material-ui/icons'
import useStyles from './styles'
import SideBarMenuItem from '@/components/organisms/SideBarMenuItem'

export type SideBarProps = {
  sideBarWidth: number
  sideBarOpened: boolean
  onSideBarClose: () => void
}

export default function SideBar({
  sideBarWidth,
  sideBarOpened,
  onSideBarClose
}: SideBarProps) {
  const classes = useStyles(sideBarWidth)()
  const theme = useTheme()

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={sideBarOpened}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={onSideBarClose}>
          {theme.direction === 'ltr' ? <ChevronLeft /> : <ChevronRight />}
        </IconButton>
      </div>
      <Divider />

      <List>
        <SideBarMenuItem
          icon={People}
          title="ユーザー管理"
          to={`/users/list`}
        />
      </List>
    </Drawer>
  )
}
