import React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '@/app/rootReducer'
import FlashMessage from '@/components/organisms/FlashMessage'

const FlashMessageSystem = () => {
  const { flash } = useSelector((state: RootState) => state.commonState)

  return flash.message ? <FlashMessage {...flash} /> : null
}

export default FlashMessageSystem
