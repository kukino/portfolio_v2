import React from 'react'
import AppContainer from './index'
import { withRedux, withAppContainer } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { flashMessage } from '@/components/ecosystem/FlashMessageSystem/index.stories'
import { DeepPartial } from '@reduxjs/toolkit'
import { initialCommonState } from '@/slicers/common'
import { withLoginState } from '@/slicers/users/mock'

const base = require('paths.macro')

export default {
  title: base.slice(0, -1),
  component: AppContainer,
  decorators: [withRedux()],
  includeStories: /.*Story$/
}

export const guestStory = () => (
  <AppContainer>
    <div>main</div>
  </AppContainer>
)

export const adminStory = () => (
  <AppContainer>
    <div>main</div>
  </AppContainer>
)

adminStory.story = {
  decorators: [withAppContainer(), withRedux(withLoginState)]
}

export const flashMessageStory = () => <div>main</div>

const flashMessageState: DeepPartial<RootState> = {
  commonState: {
    ...initialCommonState,
    flash: flashMessage
  }
}

flashMessageStory.story = {
  decorators: [
    withAppContainer(),
    withRedux({ ...withLoginState, ...flashMessageState })
  ]
}
