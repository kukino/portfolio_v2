import React from 'react'
import { Button, Menu, MenuItem, Avatar, Box } from '@material-ui/core'
import styles from './styles.module.scss'
import { RootState } from '@/app/rootReducer'
import { useSelector, useDispatch } from 'react-redux'
import { Person } from '@material-ui/icons'
import authRequest from '@/api/auth'
import { useHistory } from 'react-router'
import { getLoginUserSelector } from '@/selector/users'
import { setFlashSuccess } from '@/slicers/common'
import useStyles from './styles'

export type UserIconMenuProps = {
  textColor?: string
}

export default function UserIconMenu({
  textColor = 'black'
}: UserIconMenuProps) {
  const classes = useStyles(textColor)()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const loginUser = useSelector((state: RootState) =>
    getLoginUserSelector(state)
  )
  const dispatch = useDispatch()
  const history = useHistory()

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const handleClickProfile = () => {
    handleClose()
  }

  const handleClickChangePassword = () => {
    handleClose()
  }

  const handleClickLogout = async () => {
    await dispatch(authRequest.logout().request())
    await history.push('/login')
    await dispatch(setFlashSuccess('ログアウトしました'))
  }

  return loginUser ? (
    <div className={classes.root}>
      <Button
        aria-controls="simple-menu"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <Box display="flex" alignItems="center">
          <Avatar className={classes.avatar}>
            <Person />
          </Avatar>
          <Box ml={1} className={classes.text}>
            {loginUser.name}
          </Box>
        </Box>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        className={classes.menuContainer}
        // style={{ margin: '2rem' }}
      >
        <MenuItem onClick={handleClickProfile}>プロフィール</MenuItem>
        <MenuItem onClick={handleClickChangePassword}>パスワード変更</MenuItem>
        <MenuItem onClick={handleClickLogout}>ログアウト</MenuItem>
      </Menu>
    </div>
  ) : null
}
