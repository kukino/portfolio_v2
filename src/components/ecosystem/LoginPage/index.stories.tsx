import React from 'react'
import LoginPage from './index'
import {withRedux} from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {

}

export default {
  title: base.slice(0, -1),
  component: LoginPage,
  decorators: [withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const defaultStory = () => <LoginPage />
