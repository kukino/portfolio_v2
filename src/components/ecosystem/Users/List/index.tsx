import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '@/app/rootReducer'
import { DetailButton, AddButton } from '@/components/atoms/Buttons'
import { Link } from 'react-router-dom'
import { setLoading } from '@/slicers/common'
import usersRequest from '@/api/users'
import CustomMaterialTable from '@/components/organisms/CustomMaterialTable'
import { Box, Button } from '@material-ui/core'

type AgentCompaniesUsersListProps = {}

const UsersList: React.FC<AgentCompaniesUsersListProps> = () => {
  const dispatch = useDispatch()
  const users = useSelector((state: RootState) =>
    Object.values(state.usersState.usersById)
  )

  useEffect(() => {
    const func = async () => {
      await dispatch(setLoading(true))
      await dispatch(usersRequest.index().request())
      await dispatch(setLoading(false))
    }
    func()
  }, [])

  return (
    <CustomMaterialTable
      title="ユーザー一覧"
      data={users.map(item => ({ ...item }))}
      columns={[
        {
          field: 'id',
          title: 'ID'
        },
        {
          field: 'name',
          title: '名前'
        },
        {
          title: 'アクション',
          render: row => (
            <DetailButton component={Link} to={`/users/${row.id}/show`} />
          )
        }
      ]}
      actions={[
        {
          isFreeAction: true,
          icon: () => (
            <Box width="6rem">
              <AddButton component={Link} to={`/users/create`} />
            </Box>
          ),
          onClick: event => null
        }
      ]}
    />
  )
}

export default UsersList
