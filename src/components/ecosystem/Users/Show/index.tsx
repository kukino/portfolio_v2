import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '@/app/rootReducer'
import { useParams, useHistory } from 'react-router-dom'
import { UserFormData } from '@/components/organisms/UserForm'
import UserForm from '@/components/organisms/UserForm'
import { UsersRouteProps } from '@/App'
import usersRequest from '@/api/users'
import { shallowEqual } from '@babel/types'
import { setFlashSuccess } from '@/slicers/common'
import isequal from 'lodash.isequal'
import UsersDelete from '../Delete'

const UsersShow: React.FC = () => {
  const { user: userId } = useParams<UsersRouteProps>()
  const dispatch = useDispatch()
  const { user, response } = useSelector(
    (state: RootState) => ({
      user: state.usersState.usersById[Number(userId)],
      response: state.commonState.response
    }),
    shallowEqual
  )

  const putApi = usersRequest.put(userId)
  const history = useHistory()

  const handleSubmit = async (data: UserFormData) => {
    await dispatch(putApi.request(data))
  }

  useEffect(() => {
    const func = async () => {
      if (isequal(response, putApi.success())) {
        await dispatch(setFlashSuccess('ユーザーを編集しました'))
      }
    }
    func()
  }, [response])

  return user ? (
    <UserForm
      onSubmit={handleSubmit}
      data={{ ...user, password: '' }}
      password={false}
      backUrl={'/users/list'}
      deleteButton={<UsersDelete />}
    />
  ) : (
    <UsersDelete />
  )
}

export default UsersShow
