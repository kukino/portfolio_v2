import React from 'react'
import MaterialUIFormDialogSample from './index'
import { withRedux, withAppContainer } from '@/components/decorators'
import { RootState } from '@/app/rootReducer'
import { DeepPartial } from '@reduxjs/toolkit'
import { action } from '@storybook/addon-actions'
const base = require('paths.macro')

const preloadState: DeepPartial<RootState> = {}

export default {
  title: base.slice(0, -1),
  component: MaterialUIFormDialogSample,
  decorators: [withAppContainer(), withRedux(preloadState)],
  includeStories: /.*Story$/
}

export const defaultStory = () => (
  <MaterialUIFormDialogSample
    isOpened={true}
    onClickCancelButton={action('canceled')}
    onSubmit={action('submit')}
  />
)
