import React, { useState } from 'react'
import styles from './styles.module.scss'
import { TextField, Box } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { User } from '@/slicers/users'
import {
  EditButton,
  SaveButton,
  BackButton,
  CancelButton
} from '@/components/atoms/Buttons'
import ButtonList from '@/components/atoms/ButtonList'
import { Link } from 'react-router-dom'
import useStyles from './styles'

export type UserFormData = {
  password: string
} & User

export type LoginFormProps = {
  data?: UserFormData | User
  password: boolean
  backUrl: string
  deleteButton?: React.ReactElement
  onSubmit: (formData: UserFormData) => Promise<void>
}

enum FormType {
  CREATE,
  SHOW,
  EDIT
}

const UserForm: React.FC<LoginFormProps> = ({
  data,
  password,
  backUrl,
  deleteButton,
  onSubmit: _onSubmit
}) => {
  const classes = useStyles()
  const { register, handleSubmit, errors } = useForm<UserFormData>()
  const [loading, setLoading] = useState(false)
  const [formType, setFormType] = useState(
    data ? FormType.SHOW : FormType.CREATE
  )

  const onSubmit = handleSubmit(async props => {
    await setLoading(true)
    await _onSubmit(props)
    await setLoading(false)
    await setFormType(FormType.SHOW)
  })

  const inputProps = {
    readOnly: formType === FormType.SHOW
  }

  const Footer = () => {
    const OKButton =
      formType === FormType.SHOW ? (
        <EditButton onClick={() => setFormType(FormType.EDIT)} />
      ) : (
        <SaveButton type="submit" loading={loading} />
      )

    return (
      <Box display="flex" justifyContent="space-between" width="100%">
        <Box display="flex" justifyContent="flex-end">
          {formType === FormType.EDIT && deleteButton}
        </Box>
        <Box>
          <ButtonList>
            {formType === FormType.EDIT ? (
              <CancelButton onClick={() => setFormType(FormType.SHOW)}>
                キャンセル
              </CancelButton>
            ) : backUrl ? (
              <BackButton component={Link} to={backUrl} />
            ) : null}
            {OKButton}
          </ButtonList>
        </Box>
      </Box>
    )
  }

  return (
    <form onSubmit={onSubmit}>
      <div>
        <div className={classes.root}>
          <TextField
            required
            label="Name"
            name="name"
            defaultValue={data ? data.name : ''}
            inputRef={register({ required: true })}
            InputProps={inputProps}
          />
          {errors.name && 'name is required.'}

          {password && (
            <React.Fragment>
              <TextField
                required
                id="standard-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
                name="password"
                inputRef={register({ required: true })}
                InputProps={inputProps}
              />
              {errors.password && 'password is required.'}
            </React.Fragment>
          )}

          <TextField
            required
            label="Email"
            name="email"
            defaultValue={data ? data.email : ''}
            inputRef={register({ required: true })}
            InputProps={inputProps}
          />
          {errors.email && 'email is required.'}
        </div>
      </div>
      <div className={classes.buttonContainer}>
        <Footer />
      </div>
    </form>
  )
}

export default UserForm
