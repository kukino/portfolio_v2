import React from 'react'
import { ListItem, ListItemIcon, ListItemText, Box } from '@material-ui/core'
import { Link } from 'react-router-dom'

export type iconNames = 'inbox'

type SideBarMenuItemProps = {
  icon?: React.FC
  to: string
  title: string
}

const SideBarMenuItem: React.FC<SideBarMenuItemProps> = ({
  icon: Icon,
  title,
  to
}) => {
  return (
    <ListItem button key={to} component={Link} to={to}>
      {Icon && (
        <Box mr={1}>
          <Icon />
        </Box>
      )}
      <ListItemText primary={title} />
    </ListItem>
  )
}

export default SideBarMenuItem
