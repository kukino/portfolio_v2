import React from 'react'
import clsx from 'clsx'
import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core'
import { Menu } from '@material-ui/icons'
import useStyles from './styles'

export type AdminHeaderProps = {
  sideBarWidth: number
  children: React.ReactNode
  sideBarOpened: boolean
  onSidebarOpen: () => void
}

export default function AdminHeader({
  sideBarWidth,
  children,
  sideBarOpened,
  onSidebarOpen
}: AdminHeaderProps) {
  const classes = useStyles(sideBarWidth)()

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: sideBarOpened
      })}
    >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={onSidebarOpen}
          edge="start"
          className={clsx(classes.menuButton, sideBarOpened && classes.hide)}
        >
          <Menu />
        </IconButton>
        <div className={classes.content}>
          <Typography variant="h6" noWrap className={classes.flex_align_center}>
            <div>ポートフォリオ</div>
          </Typography>
          {children}
        </div>
      </Toolbar>
    </AppBar>
  )
}
