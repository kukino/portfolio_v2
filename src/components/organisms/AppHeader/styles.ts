import { makeStyles, Theme, createStyles } from '@material-ui/core/styles'

const useStyles = (drawerWidth: number) =>
  makeStyles((theme: Theme) =>
    createStyles({
      appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen
        })
      },
      appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
          easing: theme.transitions.easing.easeOut,
          duration: theme.transitions.duration.enteringScreen
        })
      },
      menuButton: {
        marginRight: theme.spacing(2)
      },
      hide: {
        display: 'none'
      },
      content: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%'
      },
      flex_align_center: {
        display: 'flex',
        alignItems: 'center'
      }
    })
  )

export default useStyles
