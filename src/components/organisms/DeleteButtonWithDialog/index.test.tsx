import React from 'react'
import '@testing-library/jest-dom'
import { render, fireEvent, screen, waitFor } from '@testing-library/react'
import AppWrapper from '@/AppWrapper'
import DeleteButtonWithDialog from '.'
import { sleep } from '@/utils'

jest.spyOn(window, 'alert').mockImplementation(() => {})

const setup = async () => {
  const handleSubmit = jest.fn(async () => {
    await sleep(100)
  })

  const utils = render(
    <AppWrapper>
      <DeleteButtonWithDialog onClick={handleSubmit} />
    </AppWrapper>
  )

  const dialogText = '本当に削除しますか'
  const handleAlert = (window.alert = jest.fn())

  const changeInput = (value: string) =>
    fireEvent.change(utils.getByPlaceholderText('Delete'), {
      target: { value }
    })

  const clickDialogOpenButton = () =>
    fireEvent.click(utils.getAllByRole('button')[0])

  const clickCancel = () => fireEvent.click(utils.getAllByRole('button')[0])

  const clickSubmit = () => fireEvent.click(utils.getAllByRole('button')[1])

  const findLoadingIcon = () => utils.findByTestId('loadingIcon')
  const findIcon = () => utils.findByTestId('icon')

  return {
    utils,
    handleSubmit,
    clickSubmit,
    clickCancel,
    clickDialogOpenButton,
    changeInput,
    dialogText,
    handleAlert,
    findLoadingIcon,
    findIcon
  }
}

test('opened', async () => {
  const utils = await setup()

  expect(utils.clickDialogOpenButton()).toBe(true)
  expect(screen.queryByText(utils.dialogText)).toBeInTheDocument()
})

test('canceled', async () => {
  const utils = await setup()

  expect(utils.clickDialogOpenButton()).toBe(true)
  expect(utils.clickCancel()).toBe(true)
  expect(screen.queryByText(utils.dialogText)).toBeNull()
})

test('wrong input text', async () => {
  const utils = await setup()

  expect(utils.clickDialogOpenButton())
  expect(utils.changeInput('wrong')).toBe(true)

  utils.clickSubmit()

  await waitFor(() => expect(utils.handleAlert).toHaveBeenCalled())
})

test('succcess', async () => {
  const utils = await setup()

  expect(utils.clickDialogOpenButton())
  expect(utils.changeInput('Delete')).toBe(true)
  utils.clickSubmit()

  await utils.findLoadingIcon()

  await waitFor(() => {
    expect(utils.handleSubmit).toHaveBeenCalled()
  })

  await utils.findIcon()
})
