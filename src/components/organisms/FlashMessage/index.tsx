import React from 'react'
import { Snackbar } from '@material-ui/core'
import { Alert, Color } from '@material-ui/lab'

type FlashProps = {
  type: Color
  message: string | null
}

const FlashMessage: React.FC<FlashProps> = ({ type, message }) => {
  const [open, setOpen] = React.useState(true)

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      open={open}
      autoHideDuration={6000}
      onClose={() => setOpen(false)}
    >
      <Alert onClose={() => setOpen(false)} severity={type}>
        {message}
      </Alert>
    </Snackbar>
  )
}

export default FlashMessage
