import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router'
import { initCommonStates } from '@/slicers/common'

const Common: React.FC = () => {
  const dispatch = useDispatch()
  const location = useLocation()

  useEffect(() => {
    return () => {
      dispatch(initCommonStates())
    }
  }, [location.pathname])

  return null
}

export default Common
