import React from 'react'
import { addDecorator } from '@storybook/react';
import { ThemeProvider, createMuiTheme, StylesProvider } from '@material-ui/core/styles'

addDecorator(storyFn =>
  <StylesProvider injectFirst>
    <ThemeProvider theme={createMuiTheme({})}>
      {storyFn()}
    </ThemeProvider>
  </StylesProvider>
);
